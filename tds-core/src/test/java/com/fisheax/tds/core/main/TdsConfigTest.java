/**
 * @FileName TdsConfigTest.java
 * @Package com.fisheax.tds.core.main;
 * @Author fisheax
 * @Date 8/30/2016
 */
package com.fisheax.tds.core.main;

import org.junit.Test;

/**
 * @ClassName: TdsConfigTest
 * @Description
 * @Author fisheax
 * @Date 8/30/2016
 */
public class TdsConfigTest
{
	/**************************************************
	 tds.test1=jdbc:mysql:///students
	 tds.test1.usr=develop
	 tds.test1.pwd=123456

	 #fake, would be ignored ...
	 tds.test2=jdbc:mysql:///ffcv

	 #do not load Oracle jdbc driver
	 tds.test3=jdbc:oracle:thin:@//127.0.0.1:1521/orcl
	 tds.test3.usr=fake_name
	 tds.test3.pwd=fack_pass

	 tds.test4.xxx=jdbc:mysql:///students
	 tds.test4.xxx.usr=develop
	 tds.test4.xxx.pwd=123456

	 #link name can not use empty linkname
	 tds=jdbc:mysql:///students
	 tds.usr=develop
	 tds.pwd=123456

	 #tds.default=test1
	 #tds.default=xxx
	 #
	 #tds.default=default
	 tds.default=DEFAULT
	 ***************************************************/

	/**
	 * 测试TDS配置参数是否能够正常获取
	 */
	@Test
	public void testInit()
	{
		System.out.println(TdsConfig.class.getClassLoader());
		TdsConfig.init();
		TdsConfig.printInfos();
	}
}
