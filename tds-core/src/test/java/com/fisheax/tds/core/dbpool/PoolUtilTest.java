/**
 * @FileName PoolUtilTest.java
 * @Package com.fisheax.tds.core.dbpool;
 * @Author fisheax
 * @Date 9/3/2016
 */
package com.fisheax.tds.core.dbpool;

import com.fisheax.tds.core.main.TdsConfig;
import com.fisheax.tds.core.main.TdsSwitcher;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @ClassName: PoolUtilTest
 * @Description
 * @Author fisheax
 * @Date 9/3/2016
 */
public class PoolUtilTest
{
	@Test
	public void testGetConnection() throws Exception
	{
		TdsConfig.printInfos();
		TdsSwitcher.switchTo("test1");
		TdsConfig.DbLinkInfo linkInfo = TdsConfig.getActiveLinkInfo();
		Assert.assertEquals(linkInfo.getLinkName(), "test1");

		Connection connection = PoolUtil.getConnection(linkInfo);
		Assert.assertTrue(PoolUtil.getPool(linkInfo.getLinkName()).getIdleConnCount() == 1);
		Statement st = connection.createStatement();
		ResultSet rs = st.executeQuery("select * from student_info");
		while (rs.next())
		{
			System.out.println(rs.getObject(1));
			System.out.println(rs.getObject(2));
			System.out.println(rs.getObject(3));
			System.out.println(rs.getObject(4));
		}
		connection.isClosed();
		connection.close();
		connection.isClosed();
	}
}
