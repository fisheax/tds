/**
 * @FileName DriverHandlerTest.java
 * @Package com.fisheax.tds.core.main;
 * @Author fisheax
 * @Date 8/30/2016
 */
package com.fisheax.tds.core.main;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;
import com.fisheax.tds.core.dbpool.ConnPool;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.junit.Assert;
import org.junit.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Properties;

/**
 * @ClassName: DriverHandlerTest
 * @Description
 * @Author fisheax
 * @Date 8/30/2016
 */
public class DriverHandlerTest
{
	private ComboPooledDataSource c3p0DataSource;
	private DruidDataSource druidDataSource;

	/**
	 * 测试拿到的connection对象是否已被拦截
	 */
	@Test
	public void testNonPooledConnection() throws Exception
	{
		Connection connection =
			DriverManager.getConnection("jdbc:mysql:///students?useSSL=false&useServerPrepStmts=true", "develop",
				"123456");
		testConnection(connection);
	}

	/**
	 * 测试 c3p0 是否能够正常工作
	 */
	@Test
	public void testC3p0PooledConnection() throws Exception
	{
		produceC3p0DataSource();
		testConnection(c3p0DataSource.getConnection());
	}

	@Test
	public void testDruidPooledConnection() throws Exception
	{
		produceDruidDataSource();
		testConnection(druidDataSource.getConnection());
	}

	/**
	 * 测试 PreparedStatement 缓存的情况
	 *
	 * c3p0 里拿到的 PS 对象都不会重复, 估计可能操作逻辑封装在内部了, 而不是给出相同 PS 对象
	 */
	@Test
	public void testC3p0PreparedStatement() throws Exception
	{
		// 测试思路可能不对, 拿到的 ps 对象都不相同, 应该是关乎 c3p0 内在实现的缓存策略了
		// 但是在这些 ps 里, 都没有 prepare 发送出去, 一定开启了 useServerPrepStmts 才有效果
		// 所有暂时的思路就是自己在连接的属性里设置 useServerPrepStmt 为 false
		int size = 100;

		produceC3p0DataSource();
		c3p0DataSource.setMaxStatements(size + 20);
		c3p0DataSource.setMaxStatementsPerConnection(size + 20);

		Connection connection = c3p0DataSource.getConnection();
		ArrayList<PreparedStatement> pslt = new ArrayList<>(size);

		for (int i = 0; i < size + 200; i++)
		{
			PreparedStatement ps = connection.prepareStatement("select * from student_info");
			ps.executeQuery().close();
			if (pslt.contains(ps))
				System.out.println("ps: ================== " + ps);
			pslt.add(ps);
		}
		System.out.println("==================" + pslt.size() + "=====================");
		pslt.forEach(System.out::println);
	}

	/**
	 * 测试 PreparedStatement 缓存的情况
	 *
	 * c3p0 里拿到的 PS 对象都不会重复, 估计可能操作逻辑封装在内部了, 而不是给出相同 PS 对象
	 */
	@Test
	public void testDruidPreparedStatement() throws Exception
	{
	}

	// ================================================================================================

	private void produceC3p0DataSource() throws Exception
	{
		produceC3p0DataSource("com.mysql.jdbc.Driver", "jdbc:mysql:///students?useSSL=false&useServerPrepStmts=true",
			"develop", "123456");
	}

	private void produceC3p0DataSource(String driver, String url, String name, String pass) throws Exception
	{
		this.c3p0DataSource = new ComboPooledDataSource();
		this.c3p0DataSource.setDriverClass(driver);
		this.c3p0DataSource.setJdbcUrl(url);
		this.c3p0DataSource.setUser(name);
		this.c3p0DataSource.setPassword(pass);
	}

	private void produceDruidDataSource() throws Exception
	{
		produceDruidDataSource("com.mysql.jdbc.Driver", "jdbc:mysql:///students?useSSL=false", "develop", "123456");
	}

	private void produceDruidDataSource(String driver, String url, String name, String pass) throws Exception
	{
		this.druidDataSource = ((DruidDataSource) DruidDataSourceFactory.createDataSource(new Properties()
		{
			{
				this.put("driverClassName", driver);
				this.put("url", url);
				this.put("username", name);
				this.put("password", pass);
			}
		}));
	}

	private void testConnection(Connection connection) throws Exception
	{
		printDrivers();

		PreparedStatement ps = connection.prepareStatement("select * from student_info");
		ResultSet rs = ps.executeQuery();
		printStudentInfoRS(rs, ps.getMetaData().getColumnCount());

		// 切换到test1上执行
		TdsSwitcher.switchTo("test1");
		ps = connection.prepareStatement("select * from student_info");
		rs = ps.executeQuery();
		printStudentInfoRS(rs, ps.getMetaData().getColumnCount());

		if (TdsSwitcher.switchTo("test_fake"))
		{
			// 不会执行
			ps = connection.prepareStatement("select * from student_info");
			rs = ps.executeQuery();
			printStudentInfoRS(rs, ps.getMetaData().getColumnCount());
		}

		// 切换到DEFAULT上执行
		TdsSwitcher.switchTo("DEFAULT");
		ps = connection.prepareStatement("select * from time_test");
		rs = ps.executeQuery();
		printStudentInfoRS(rs, ps.getMetaData().getColumnCount());

		ConnPool pool = ConnPool.getPool("test1");
		if (pool != null)
		{
			Assert.assertEquals(1, pool.getIdleConnCount());
			Assert.assertEquals(1, pool.getUsedConnCount());
		}

		rs.close();
		ps.close();
		System.out.println(connection.isClosed());
		connection.close();
		System.out.println(connection.isClosed());

		// 通过关闭连接池来关闭连接池中的连接, c3p0 中 close 操作是异步的
		if (c3p0DataSource != null)
		{
			c3p0DataSource.close();
			Thread.sleep(1000);
		}

		if (druidDataSource != null)
		{
			druidDataSource.close();
			Thread.sleep(1000);
		}
		pool = ConnPool.getPool("test1");
		if (pool != null)
		{
			Assert.assertEquals(2, pool.getIdleConnCount());
			Assert.assertEquals(0, pool.getUsedConnCount());
		}
	}

	private void printStudentInfoRS(ResultSet rs, int count) throws Exception
	{
		while (rs.next())
		{
			for (int i = 1; i <= count; i++)
				System.out.println(rs.getObject(i));
		}
		System.out.println("=======================================");
	}

	private void printDrivers()
	{
		Enumeration<Driver> drivers = DriverManager.getDrivers();
		while (drivers.hasMoreElements())
			System.out.println(drivers.nextElement());
	}
}
