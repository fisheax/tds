/**
 * @FileName TestJavassist.java
 * @Package com.fisheax.tds.core.javassist;
 * @Author fisheax
 * @Date 8/27/2016
 */
package com.fisheax.tds.others.dependency;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import org.junit.Assert;
import org.junit.Test;

import java.sql.*;
import java.util.Enumeration;
import java.util.Optional;

/**
 * @ClassName: TestJavassist
 * @Description
 * @Author fisheax
 * @Date 8/27/2016
 */
public class TestJavassist
{
	/**
	 * 测试应用存在javassist, TDS是否会对目标应用产生影响
	 */
	@Test
	public void testJavassistDenpendency() throws Exception
	{
		Connection connection = DriverManager.getConnection("jdbc:mysql:///students?useSSL=false", "develop", "123456");
		PreparedStatement ps = connection.prepareStatement("select * from student_info");
		ResultSet rs = ps.executeQuery();
		printStudentInfoRS(rs, ps.getMetaData().getColumnCount());

		System.out.println("The Class [ClassPool] 's Classloader Is : " + ClassPool.class.getClassLoader());

		// in application
		ClassPool pool = ClassPool.getDefault();
		CtClass clazz = pool.get("com.fisheax.tds.others.dependency.TestJavassist$HelloWorld");
		CtMethod helloMethod = clazz.getDeclaredMethod("hello");
		helloMethod.insertBefore("System.out.println(\"Intercept In Test\");");

		HelloWorld hw = ((HelloWorld) clazz.toClass().newInstance());
		hw.hello();

		rs.close();
		ps.close();
		connection.close();
	}

	/**
	 * 测试能否拦截DriverManager中的静态方法
	 * 存在 Java 访问的安全问题
	 * @throws Exception
	 */
	@Test
	public void testJavassistMakeClass() throws Exception
	{
		ClassPool pool = ClassPool.getDefault();
		CtClass manager = pool.get("java.sql.DriverManager");
		byte[] clazz = manager.toBytecode();
		Assert.assertNotNull(clazz);
	}

	/**
	 * 测试Driver的拦截是否正常
	 * @throws Exception
	 */
	@Test
	public void testDriverInteceptor() throws Exception
	{
		// 触发driver的加载
		Class.forName("com.mysql.jdbc.Driver");

		Enumeration<Driver> drivers = DriverManager.getDrivers();
		Optional.ofNullable(drivers).ifPresent(ds -> {
			int count = 0;
			for (; ds.hasMoreElements(); count++)
				System.out.println(ds.nextElement().getClass().getName());

			System.out.println("Drivers Count " + count);
		});
	}

	public static class HelloWorld
	{
		public void hello()
		{
			System.out.println("Hello World");
		}
	}

	private void printStudentInfoRS(ResultSet rs, int count) throws Exception
	{
		while (rs.next())
		{
			for (int i = 1; i <= count; i++)
				System.out.println(rs.getObject(i));
		}
	}
}
