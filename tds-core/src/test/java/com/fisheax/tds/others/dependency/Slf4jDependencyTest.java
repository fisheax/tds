/**
 * @FileName Slf4jDependencyTest.java
 * @Package com.fisheax.tds.others.dependency;
 * @Author fisheax
 * @Date 9/5/2016
 */
package com.fisheax.tds.others.dependency;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * @ClassName: Slf4jDependencyTest
 * @Description 测试当应用中出现Slf4j的依赖是否能够正常使用
 * @Author fisheax
 * @Date 9/5/2016
 */
public class Slf4jDependencyTest
{
	@Test
	public void testSlf4j() throws Exception
	{
		Connection connection = DriverManager.getConnection("jdbc:mysql:///students?useSSL=false", "develop", "123456");
		PreparedStatement ps = connection.prepareStatement("select * from student_info");
		ResultSet rs = ps.executeQuery();
		printStudentInfoRS(rs, ps.getMetaData().getColumnCount());

		System.out.println("The Class [Logger] 's Classloader Is : " + Logger.class.getClassLoader());
		System.out.println("The Class [LoggerFactory] 's Classloader Is : " + LoggerFactory.class.getClassLoader());

		// in application
		Logger logger = LoggerFactory.getLogger(Slf4jDependencyTest.class);

		logger.trace("trace");
		logger.debug("debug");
		logger.info("info");
		logger.warn("warn");
		logger.error("error");

		rs.close();
		ps.close();
		connection.close();
	}

	private void printStudentInfoRS(ResultSet rs, int count) throws Exception
	{
		while (rs.next())
		{
			for (int i = 1; i <= count; i++)
				System.out.println(rs.getObject(i));
		}
	}
}
