/**
 * @FileName ConnPool.java
 * @Package com.fisheax.tds.core.dbpool;
 * @author fisheax
 * @date 8/21/2016
 */
package com.fisheax.tds.core.dbpool;

import com.fisheax.tds.core.exception.PoolInitializeException;
import com.fisheax.tds.core.main.TdsConfig;
import com.fisheax.tds.core.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author fisheax
 * @ClassName: ConnPool
 * @Description 连接池实现
 * @date 8/21/2016
 */
public class ConnPool
{
	private static Logger logger = LoggerFactory.getLogger(ConnPool.class);

	// 所有连接池
	private static Map<String, ConnPool> pools = new ConcurrentHashMap<>();

	// 连接池名称
	private String poolName = "POOL_FAKE_NAME";
	// 默认初始化2个数据库连接
	private int poolInitSize = 2;
	// 是否启用空闲连接回收, 默认关闭
	// 没有实现好的策略, 暂时10分钟定时回收一次
	private boolean enableRecycle = false;

	private String url = "";
	private String usr = "";
	private String pwd = "";

	private Driver driver = null;

	// 空闲Connection
	private LinkedList<Connection> idleConnections = new LinkedList</*Connection*/>()
	/*{
		// implemented in DriverHandler
		@Override
		public boolean add(Connection proxy)
		{
			ConnHandler handler = ((ConnHandler) Proxy.getInvocationHandler(proxy));
			handler.setTdsPool(ConnPool.this);
			return super.add(proxy);
		}
	}*/;

	// 已分配的Connection
	private LinkedList<Connection> usedConnections = new LinkedList<Connection>()
	{
		@Override
		public boolean add(Connection connection)
		{
			return connection != null && super.add(connection);
		}
	};

	/**
	 * 构造连接池
	 * @param poolName
	 * @param poolInitSize
	 * @param enableRecycle
	 * @param url 数据库连接URL
	 * @param username
	 * @param password
	 */
	public ConnPool(String poolName, int poolInitSize, boolean enableRecycle,
	                String url, String username, String password) throws PoolInitializeException
	{
		this.poolName = StringUtil.isBlank(poolName) ? getRndPoolName() : poolName;
		this.enableRecycle = enableRecycle;
		if (poolInitSize <= 0)
			logger.warn("The Paramter PoolInitSize Is Invalid, Change It Into Default Size (2) ...");

		if (StringUtil.isBlank(url))
			throw new PoolInitializeException("The JDBC URL Could Not Be Blank ...");

		this.url = url;
		this.usr = username;
		this.pwd = password;

		pools.put(poolName, this);
	}

	/**
	 * 连接池初始化
	 */
	private void initConnectionPool() throws PoolInitializeException
	{
		Enumeration<Driver> drivers = DriverManager.getDrivers();

		if (drivers != null)
			while (drivers.hasMoreElements())
			{
				Driver d = drivers.nextElement();
				try
				{
					if (d.acceptsURL(this.url))
					{
						this.driver = d;
						for (int i = 0; i < poolInitSize; i++)
						{
							Connection connection = getConnByDriver();

							if (connection != null)
								this.idleConnections.add(connection);
						}
						break;
					}
				}
				catch (SQLException e) { }
			}

		if (this.driver == null)
			throw new PoolInitializeException("No Driver Registered To Do With Gived JDBC URL ...");

		// 默认定时10分钟进行一次清理
		if (enableRecycle)
			startRecycle(10);
	}

	/**
	 * 通过Driver获取连接
	 * @return
	 */
	private Connection getConnByDriver()
	{
		try
		{
			return this.driver.connect(this.url, new Properties() {
				{
					this.put("user", ConnPool.this.usr);
					this.put("password", ConnPool.this.pwd);
				}
			});
		}
		catch (SQLException e)
		{
			logger.warn("Error While Retriving The Connection ...", e);
			return null;
		}
	}

	/**
	 * 获取数据连接
	 * @return
	 */
	protected Connection getConnection()
	{
		// lazy load
		if (idleConnections.isEmpty() && usedConnections.isEmpty())
			initConnectionPool();

		Connection conn = idleConnections.pollFirst();

		if (conn == null)
		{
			addConnection();
			conn = idleConnections.pollFirst();
		}

		usedConnections.add(conn);
		return conn;
	}

	/**
	 * 增加连接, 采用成倍增加策略
	 * 应当处理数据库连接数极限的情况, 不是想要add就会有的
	 * 这里就不处理了. 一般应用正常逻辑下也不会遇上这种问题
	 */
	private void addConnection()
	{
		int total = usedConnections.size();
		for (int count = 0; count < total; count++)
			this.idleConnections.add(getConnByDriver());
	}

	/**
	 * 回收连接
	 * @param proxy 原始数据连接的代理对象
	 */
	public synchronized void recycleConn(Connection proxy)
	{
		this.idleConnections.add(proxy);

		// 不能用这个remove方法来删除
		// 因为它会调用equals方法去判定是否是同一对象
		// 而这样又会走入ConnHandler#invoke的逻辑
		// 当切换到DEFAULT上的时候PoolUtil#getConnection拿不到结果, NullPointException
		// 改用直接遍历, 使用 == 来判定是否为同一对象
		// this.usedConnections.remove(proxy);

		LinkedList<Connection> usedConnections = new LinkedList<>();

		this.usedConnections.stream()
			.forEach(c -> {
				if (c != proxy)
					usedConnections.add(c);
			});

		this.usedConnections = usedConnections;
	}

	/**
	 * 获取连接池名称
	 * @return
	 */
	public String getPoolName()
	{
		return poolName;
	}

	/**
	 * 设置连接池初始化大小
	 * @return
	 */
	public int getPoolInitSize()
	{
		return poolInitSize;
	}

	/**
	 * 获取是否开启连接回收机制
	 * 暂未实现
	 * @return
	 */
	public boolean isEnableRecycle()
	{
		return enableRecycle;
	}

	/**
	 * 获取数据库连接URL
	 * @return
	 */
	public String getUrl()
	{
		return url;
	}

	/**
	 * 设置数据库连接URL, 初始化后就不允许修改
	 * @param url
	 */
	protected void setUrl(String url)
	{
		if (StringUtil.isBlank(this.url))
			this.url = url;
	}

	/**
	 * 获取数据库连接用户名称
	 * @return
	 */
	public String getUsr()
	{
		return usr;
	}

	/**
	 * 设置数据库连接用户名称, 初始化后就不允许修改
	 * @param usr
	 */
	protected void setUsr(String usr)
	{
		if (StringUtil.isBlank(this.usr))
			this.usr = usr;
	}

	/**
	 * 获取数据库连接用户密码
	 * @return
	 */
	public String getPwd()
	{
		return pwd;
	}

	/**
	 * 设置数据库连接用户密码, 初始化后就不允许修改
	 * @param pwd
	 */
	protected void setPwd(String pwd)
	{
		if (StringUtil.isBlank(this.pwd))
			this.pwd = pwd;
	}

	/**
	 * 获取当前连接使用的Driver
	 * @return
	 */
	public Driver getDriver()
	{
		return driver;
	}

	/**
	 * 获取当前所有空闲的连接
	 * @return
	 */
	protected LinkedList<Connection> getIdleConnections()
	{
		return idleConnections;
	}

	/**
	 * 当前空闲连接数
	 * @return
	 */
	public int getIdleConnCount()
	{
		return idleConnections.size();
	}

	/**
	 * 获取当前所有在使用的连接
	 * @return
	 */
	protected LinkedList<Connection> getUsedConnections()
	{
		return usedConnections;
	}

	/**
	 * 当前已使用连接数
	 * @return
	 */
	public int getUsedConnCount()
	{
		return usedConnections.size();
	}

	/**
	 * 获取所有的连接池
	 * @return
	 */
	public static Map<String, ConnPool> getPools()
	{
		return pools;
	}

	/**
	 * 获取指定的连接池
	 * @param poolName
	 * @return
	 */
	public static ConnPool getPool(String poolName)
	{
		if (StringUtil.isBlank(poolName))
			return null;

		return pools.get("POOL_" + poolName.toUpperCase());
	}

	/**
	 * 初始化所有的连接池
	 * @return
	 */
	protected static void initPools()
	{
		Map<String, TdsConfig.DbLinkInfo> infos = TdsConfig.getLinkInfos();
		// get trigger initialize
		infos.values().stream()
			.filter(s -> !"DEFAULT".equals(s.getLinkName()))
			.forEach(PoolUtil::getPool);
	}

	/**
	 * 开始定时回收
	 * @param minutes 时间间隔, 以分钟为单位
	 */
	protected static void startRecycle(int minutes)
	{
		if (minutes <= 0)
		{
			logger.warn("The Parameter Minute Should Not Less Or Equal Then Zero ...");
			return;
		}

		new Timer(true).schedule(new RecycleIdleConn(), minutes * 60 * 1000);
	}

	/**
	 * 产生随机的连接池名
	 * @return
	 */
	private static String getRndPoolName()
	{
		return "POOL_RND_" + UUID.randomUUID().toString().replace("-", "").toUpperCase();
	}
}

class RecycleIdleConn extends TimerTask
{
	/**
	 * 定时关闭空闲的数据库连接
	 */
	@Override
	public void run()
	{
		ConnPool.getPools().values().stream()
			.filter(p -> p.getIdleConnections().size() == 0)
			.collect(Collectors.toList())
			.forEach(p -> p.getIdleConnections().forEach(PoolUtil::closeConnection));
	}
}
