/**
 * @FileName StringUtil.java
 * @Package com.fisheax.tds.core.util;
 * @author fisheax
 * @date 8/22/2016
 */
package com.fisheax.tds.core.util;

/**
 * @author fisheax
 * @ClassName: StringUtil
 * @Description String的一些常用操作
 * @date 8/22/2016
 */
public class StringUtil
{
	/**
	 * 字符串是否为空
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str)
	{
		return str == null || str.length() == 0;
	}

	/**
	 * 字符串是否只有空白符组成
	 * @param str
	 * @return
	 */
	public static boolean isBlank(String str)
	{
		return str == null || str.trim().length() == 0;
	}

	/**
	 * 判断两个string是否不相等
	 * @param str1
	 * @param str2
	 * @return
	 */
	public static boolean notEquals(String str1, String str2)
	{
		if (str1 == null)
			return  false;

		return !str1.equals(str2);
	}
}
