/**
 * @FileName ConnHandler.java
 * @Package com.fisheax.tds.core.dbpool;
 * @author fisheax
 * @date 8/21/2016
 */
package com.fisheax.tds.core.main;

import com.fisheax.tds.core.dbpool.ConnPool;
import com.fisheax.tds.core.dbpool.PoolUtil;
import com.fisheax.tds.core.exception.TdsRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author fisheax
 * @ClassName: ConnHandler
 * @Description Connection代理处理器
 * @date 8/21/2016
 */
public class ConnHandler implements InvocationHandler
{
	private static Logger logger = LoggerFactory.getLogger(ConnHandler.class);

	// 所有需要用到的Connection对象, 懒加载
	// 对于DEFAULT而言, 其中的value是原始Connection对象
	// 对于TDS连接而言, 其中的value还是一个Connection代理对象
	private Map<String, Connection> connections = new HashMap<String, Connection>()
	{
		@Override
		public Connection get(Object key)
		{
			Optional<Object> conn = Optional.ofNullable(super.get(key));
			String sKey = ((String) key);

			if (conn.isPresent())
				return ((Connection) conn.get());
			else
			{
				Connection newConn = PoolUtil.getConnection(sKey);
				this.put(sKey, newConn);
				return newConn;
			}
		}
	};
	// 原Connection对象
	private Connection formerConn = null;

	// 由于JDK的动态代理的实现机制, 只能将连接池处理的部分放在一起
	// TDS连接所属连接池
	private ConnPool tdsPool = null;
	// TDS连接是否关闭
	private boolean tdsClose = false;

	/**
	 * 构造代理connection对象
	 * @param connection
	 */
	private ConnHandler(Connection connection, ConnPool pool)
	{
		if (connection == null)
			throw new IllegalArgumentException("The Former Connection Can NOT Be NULL ...");

		this.formerConn = connection;
		this.tdsPool = pool;

		// init all links info
		this.connections.put(TdsConfig.getActiveLinkName(), connection);
		TdsConfig.getLinkInfos().keySet().stream()
			.filter((String s) -> !"DEFAULT".equals(s))
			.collect(Collectors.toList())
			.forEach(k -> connections.put(k, null));
	}

	/**
	 * 拦截指定方法
	 * @param proxy
	 * @param method
	 * @param args
	 * @return
	 * @throws Throwable
	 */
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
	{
		String active = TdsConfig.getActiveLinkName();
		switch (method.getName())
		{
			case "close":
				// remind: the connections created by TDS also has intercepted with ConnHandler
				// but it doesn't matter because of it only has its "formerConn"
				// act just like a common connection object as if without existing ConnHandler

				// close all connections whether current connection is in "DEFAULT" or in "TDS"
				return closeConnection();

			case "isClosed":
				return this.tdsPool == null ? method.invoke(formerConn, args) : this.tdsClose;

			default:
				return method.invoke(tdsPool != null && tdsPool.getPoolName().substring(5).equals(active.toUpperCase()) ?
					formerConn : connections.get(active), args);
		}
	}

	// 关闭连接
	private Object closeConnection()
	{
		// 1. close "formerConn", if closed depends on target application
		PoolUtil.closeConnection(formerConn);

		// 2.1 close all TDS connections back to ConnPool
		this.connections.values().stream()
			.filter(c -> c != null && Proxy.isProxyClass(c.getClass()))
			.collect(Collectors.toList())
			.forEach(c -> {
				ConnHandler handler = ((ConnHandler) Proxy.getInvocationHandler(c));
				if (handler.getTdsPool() != null)
				{
					handler.tdsClose = true;
					handler.tdsPool.recycleConn(c);
				}
			});

		// 2.2 empty connections created by TDS
		this.connections.keySet().stream()
			.filter(k -> !"DEFAULT".equals(k))
			.collect(Collectors.toList())
			.forEach(k -> connections.put(k, null));

		return null;
	}

	/**
	 * 返回最原始的connecting对象
	 * @return
	 */
	public Connection getFormerConn()
	{
		return this.formerConn;
	}

	/**
	 * 如果当前是TDS连接, 返回所属连接池
	 * 否则抛异常
	 * @throws TdsRuntimeException
	 * @return
	 */
	public ConnPool getTdsPool()
	{
		if (this.tdsPool == null)
			throw new TdsRuntimeException("Current Connection Is Not A TDS Connection ...");

		return tdsPool;
	}

	/**
	 * 设置连接所属连接池
	 * @param tdsPool
	 */
	public void setTdsPool(ConnPool tdsPool)
	{
		if (this.tdsPool == null)
			throw new TdsRuntimeException("Current Connection Is Not A TDS Connection ...");

		this.tdsPool = tdsPool;
	}

	/**
	 * 如果当前连接是TDS连接, 返回连接是否关闭
	 * 否则抛出异常
	 * @return
	 * @throws TdsRuntimeException
	 */
	public boolean isTdsClose() throws TdsRuntimeException
	{
		if (this.tdsPool == null)
			throw new TdsRuntimeException("Current Connection Is Not A TDS Connection ...");

		return tdsClose;
	}

	/**
	 * 获取Connection的代理对象
	 * @param conn
	 * @return
	 */
	public static Connection getProxiedConn(Connection conn, ConnPool pool)
	{
		return ((Connection) Proxy.newProxyInstance(ConnHandler.class.getClassLoader(),
			new Class[]{ Connection.class }, new ConnHandler(conn, pool)));
	}
}
