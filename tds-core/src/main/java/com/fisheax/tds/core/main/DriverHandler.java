/**
 * @FileName DriverHandler.java
 * @Package com.fisheax.tds.core.main;
 * @Author fisheax
 * @Date 8/28/2016
 */
package com.fisheax.tds.core.main;

import com.fisheax.tds.core.dbpool.PoolUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.Driver;
import java.util.Properties;

/**
 * @ClassName: DriverHandler
 * @Description Driver的拦截类
 * @Author fisheax
 * @Date 8/28/2016
 */
public class DriverHandler implements InvocationHandler
{
	private static Logger logger = LoggerFactory.getLogger(DriverHandler.class);

	// 被拦截的Driver对象
	private Driver formerDriver;

	/**
	 * 构造Driver代理对象
	 * @param formerDriver
	 */
	private DriverHandler(Driver formerDriver)
	{
		if (formerDriver == null)
			throw new IllegalArgumentException("The Former Driver Can NOT Be NULL ...");

		this.formerDriver = formerDriver;
		logger.debug("Driver " + formerDriver.getClass().getName() + " Registered");
	}

	/**
	 * 拦截Driver中的connect方法
	 * 对所有获取到的Connection对象加上一层包装
	 * 注意: Driver 要是在后面又被拦截的话会出问题
	 *      至于什么问题, 看其他拦截的逻辑了,
	 *      这个问题不好解决, 因为总有逻辑可以加在 TDS 之前
	 *      现在已知的比如阿里 Druid 连接池的实现, 就会用 MockDriver 去拦截原始 Driver
	 * @param proxy
	 * @param method
	 * @param args
	 * @return
	 * @throws Throwable
	 */
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
	{
		logger.debug("Intercepted Method :" + formerDriver.getClass().getName() + "#" + method.getName());

		// only intercept connect method
		// and modify the config property, change / add "useServerPrepStmts=false"
		if ("connect".equals(method.getName()))
		{
			String active = TdsConfig.getActiveLinkName();
			if (args[1] != null)
			{
				Properties properties = ((Properties) args[1]);
				properties.setProperty("useServerPrepStmts", "false");
			}

			Connection connection = (Connection) method.invoke(formerDriver, args);
			return connection != null ? ConnHandler.getProxiedConn(connection,
				"DEFAULT".equals(active) ? null : PoolUtil.getPool(active)) : null;
		}
		else
			return method.invoke(formerDriver, args);
	}

	/**
	 * 获取Driver的代理对象
	 * @param driver
	 * @return
	 */
	public static Driver getProxiedDriver(Driver driver)
	{
		return ((Driver) Proxy.newProxyInstance(DriverHandler.class.getClassLoader(),
			new Class[]{ Driver.class }, new DriverHandler(driver)));
	}
}
