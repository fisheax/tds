/**
 * @FileName TdsClassLoader.java
 * @Package com.fisheax.tds.core;
 * @Author fisheax
 * @Date 9/9/2016
 */
package com.fisheax.tds.core.main;

import java.net.URL;

/**
 * @ClassName: TdsClassLoader
 * @Description 用于类Web环境(多ClassLoader)中, 加载需要的数据库驱动jar包
 * @Author fisheax
 * @Date 9/9/2016
 */
public class TdsClassLoader extends ClassLoader
{
	/**
	 * 构造
	 */
	public TdsClassLoader() {}

	/**
	 * 构造
	 * @param classLoader
	 */
	public TdsClassLoader(ClassLoader classLoader)
	{
		super(classLoader);
	}

	/**
	 * 根据System.properties来寻找目标类
	 * @param name
	 * @return
	 * @throws ClassNotFoundException
	 */
	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException
	{
		return super.findClass(name);
	}

	/**
	 * 根据System.properties来寻找目标资源
	 * @param name
	 * @return
	 */
	@Override
	protected URL findResource(String name)
	{
		return super.findResource(name);
	}
}
