/**
 * @FileName PoolInitializeException.java
 * @Package com.fisheax.tds.core.dbpool;
 * @author fisheax
 * @date 8/22/2016
 */
package com.fisheax.tds.core.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author fisheax
 * @ClassName: PoolInitializeException
 * @Description
 * @date 8/22/2016
 */
public class PoolRuntimeException extends RuntimeException
{
	private static Logger logger = LoggerFactory.getLogger(PoolRuntimeException.class);

	public PoolRuntimeException(String message)
	{
		super(message);
		logger.error(message);
	}

	public PoolRuntimeException()
	{
	}

	public PoolRuntimeException(String message, Throwable cause)
	{
		super(message, cause);
		logger.error(message);
	}
}
