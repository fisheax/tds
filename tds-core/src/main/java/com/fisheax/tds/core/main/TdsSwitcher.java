/**
 * @FileName TdsSwitcher.java
 * @Package com.fisheax.tds.core;
 * @author fisheax
 * @date 8/21/2016
 */
package com.fisheax.tds.core.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author fisheax
 * @ClassName: TdsSwitcher
 * @Description Connection切换接口
 * @date 8/21/2016
 */
public class TdsSwitcher
{
	private static Logger logger = LoggerFactory.getLogger(TdsSwitcher.class);

	/**
	 * 切换到指定的数据库连接上
	 * @param linkName
	 * @return
	 */
	public synchronized static boolean switchTo(String linkName)
	{
		try
		{
			TdsConfig.setActiveLinkName(linkName);
		}
		catch (IllegalArgumentException e)
		{
			logger.error(e.getMessage());
			return false;
		}

		return true;
	}
}
