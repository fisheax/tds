/**
 * @FileName PoolUtil.java
 * @Package com.fisheax.tds.core.dbpool;
 * @author fisheax
 * @date 8/21/2016
 */
package com.fisheax.tds.core.dbpool;

import com.fisheax.tds.core.main.TdsConfig;
import com.fisheax.tds.core.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.util.Map;

/**
 * @author fisheax
 * @ClassName: PoolUtil
 * @Description 连接池操作接口
 * @date 8/21/2016
 */
public class PoolUtil
{
	private static Logger logger = LoggerFactory.getLogger(PoolUtil.class);

	// 初始化所有的连接池
	static { ConnPool.initPools(); }

	/**
	 * 获取TDS的数据库连接
	 * @param info
	 * @return
	 */
	public static Connection getConnection(TdsConfig.DbLinkInfo info)
	{
		return info == null ? null : getPool(info.getLinkName()).getConnection();
	}

	/**
	 * 获取TDS的数据库连接
	 * @param linkName
	 * @return
	 */
	public static Connection getConnection(String linkName)
	{
		return StringUtil.isBlank(linkName) ? null : getPool(linkName).getConnection();
	}

	/**
	 * 关闭数据库原始连接对象, 不抛出任何异常
	 * @param connection
	 */
	public static void closeConnection(Connection connection)
	{
		try
		{
			if (!connection.isClosed())
				connection.close();
		}
		catch (Exception ex) { logger.debug("Exception While Closing The Connection ...", ex);}
	}

	/**
	 * 生成一个连接池, 使用默认名称, 默认初始化大小, 默认关闭空闲时回收
	 * @param url
	 * @param usr
	 * @param pwd
	 * @return
	 */
	public static ConnPool getPool(String url, String usr, String pwd)
	{
		return getPool(null, url, usr, pwd);
	}

	/**
	 * 生成一个连接池, 使用默认名称, 默认初始化大小, 默认关闭空闲时回收
	 * @param info
	 * @return
	 */
	public static ConnPool getPool(TdsConfig.DbLinkInfo info)
	{
		return info != null && info.getURL().isPresent() ? getPool("POOL_" + info.getLinkName().toUpperCase(),
			info.getURL().get(), info.getUsername(), info.getPassword()) : null;
	}

	/**
	 * 生成一个连接池, 默认初始化大小, 默认关闭空闲时回收
	 * @param poolName
	 * @param url
	 * @param usr
	 * @param pwd
	 * @return
	 */
	public static ConnPool getPool(String poolName, String url, String usr, String pwd)
	{
		return getPool(poolName, 2, url, usr, pwd);
	}

	/**
	 * 生成一个连接池, 默认关闭空闲时回收
	 * @param poolName
	 * @param poolInitSize
	 * @param url
	 * @param usr
	 * @param pwd
	 * @return
	 */
	public static ConnPool getPool(String poolName, int poolInitSize, String url, String usr, String pwd)
	{
		return getPool(poolName, poolInitSize, false, url, usr, pwd);
	}

	/**
	 * 生成一个连接池
	 * @param poolName
	 * @param poolInitSize
	 * @param enableRecycle
	 * @param url
	 * @param usr
	 * @param pwd
	 * @return
	 */
	public static ConnPool getPool(String poolName, int poolInitSize, boolean enableRecycle, String url, String usr, String pwd)
	{
		return new ConnPool(poolName, poolInitSize, enableRecycle, url, usr, pwd);
	}

	/**
	 * 获取所有的连接池
	 * @return
	 */
	public static Map<String, ConnPool> getPools()
	{
		return ConnPool.getPools();
	}

	/**
	 * 获取指定的连接池
	 * @param poolName
	 * @return
	 */
	public static ConnPool getPool(String poolName)
	{
		return ConnPool.getPool(poolName);
	}
}
