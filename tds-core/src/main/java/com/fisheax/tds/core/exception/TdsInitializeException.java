/**
 * @FileName TdsInitializeException.java
 * @Package com.fisheax.tds.core;
 * @author fisheax
 * @date 8/21/2016
 */
package com.fisheax.tds.core.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author fisheax
 * @ClassName: TdsInitializeException
 * @Description 运行中可能会产生的错误
 * @date 8/21/2016
 */
public class TdsInitializeException extends RuntimeException
{
	private static Logger logger = LoggerFactory.getLogger(TdsInitializeException.class);

	public TdsInitializeException()
	{
	}

	public TdsInitializeException(String message)
	{
		super(message);
		logger.error(message);
	}

	public TdsInitializeException(String message, Throwable cause)
	{
		super(message, cause);
		logger.error(message);
	}
}
