/**
 * @FileName TdsConfig.java
 * @Package com.fisheax.tds.core;
 * @author fisheax
 * @date 8/21/2016
 */
package com.fisheax.tds.core.main;


import com.fisheax.tds.core.exception.TdsInitializeException;
import com.fisheax.tds.core.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Stream;

/**
 * @author fisheax
 * @ClassName: TdsConfig
 * @Description Agent初始化配置类
 * @date 8/21/2016
 */
public class TdsConfig
{
	private static Logger logger = LoggerFactory.getLogger(TdsConfig.class);

	// TDS初始化错误标志, 为true时退出TDS, 但不影响原应用运行
	private static boolean tdsError = false;
	// 当前激活的连接名称
	private static String activeLinkName = "DEFAULT";
	// 所有数据库连接配置信息
	private static Map<String, DbLinkInfo> linkInfos = new HashMap<>();

	/**
	 * 初始化TDS配置
	 */
	public static void init()
	{
		if (!tdsError)
			try
			{
				linkInfos = getDbLinks();
				// due to the target application may using multiple datasource
				// so, if the active link is "DEFAULT" then make it an empty DbLinkInfo
				linkInfos.put("DEFAULT", new DbLinkInfo("", "", "", "DEFAULT"));
			}
			catch (TdsInitializeException e)
			{
				tdsError = true;
				linkInfos.clear();
				logger.error("Error While Initialize The TDS, Exiting TDS ...", e);
			}
	}

	/**
	 * 获取系统属性中所有配置的数据库连接
	 * 对于无效的, 仅仅忽略
	 * @return
	 */
	private static Map<String, DbLinkInfo> getDbLinks() throws TdsInitializeException
	{
		Properties properties = new Properties();
		try
		{
			// load config file pointed by property "tds.config.file"
			// prior than file "tds.properties" in classpath
			String configFilePath = System.getProperty("tds.config.file");
			File configFile;
			if (!StringUtil.isBlank(configFilePath) && (configFile = new File(configFilePath)).exists())
				properties.load(new FileInputStream(configFile));
			else
				properties.load(ClassLoader.getSystemClassLoader().getResourceAsStream("tds.properties"));
		}
		catch (IOException e)
		{
			tdsError = true;
			throw new TdsInitializeException("Error While Load The TDS Config File, Exiting TDS ...", e);
		}

		Map<String, DbLinkInfo> links = new HashMap<>();

		properties.forEach((key, value) -> {
			String sKey = ((String) key);
			String sValue = ((String) value);

			if (sKey.equals("tds.default") && !StringUtil.isBlank(sValue))
				activeLinkName = sValue;

			if (sKey.startsWith("tds.") && sValue.startsWith("jdbc:"))
				// all common JDBC URL always start with "jdbc:"
				makeDbLinkInfo(properties, sKey, links);
		});

		// if "tds.default" not given or invalid, use "DEFAULT"
		// what means using target application's connection by default
		if (!"DEFAULT".equals(activeLinkName) && !links.containsKey(activeLinkName))
			activeLinkName = "DEFAULT";

		return links;
	}

	/**
	 * 生成一个有效的连接信息
	 * @param links
	 */
	private static void makeDbLinkInfo(Properties properties, String linkKey, Map<String, DbLinkInfo> links)
	{
		// "DEFAULT" is specified, so "tds.DEFAULT" would be ignored
		if ("tds.DEFAULT".equals(linkKey))
		{
			logger.debug("The Link With Name \"" + linkKey + "\" Is Ignored ...");
			return;
		}

		String url = properties.getProperty(linkKey);
		String usr = properties.getProperty(linkKey + ".usr");
		String pwd = properties.getProperty(linkKey + ".pwd");

		// I think the username & password should not be empty
		// If so, would be ignored
		if (Stream.of(url, usr, pwd).filter(StringUtil::isBlank).count() > 0)
			return;

		if (checkURLValidatity(url))
			links.put(linkKey.substring(4), new DbLinkInfo(url, usr, pwd, linkKey.substring(4)));
	}

	/**
	 * 检查JDBC连接的有效性
	 * @param url
	 * @return
	 */
	private static boolean checkURLValidatity(String url)
	{
		Enumeration<Driver> drivers = DriverManager.getDrivers();

		try
		{
			if (drivers != null)
				while (drivers.hasMoreElements())
					if (drivers.nextElement().acceptsURL(url))
						return true;
		}
		catch (SQLException ex){ return false; }

		return false;
	}

	/**
	 * 设置TDS初始化过程出错标志
	 * @param tdsError
	 */
	public static synchronized void setTdsError(boolean tdsError)
	{
		TdsConfig.tdsError = tdsError;
	}

	/**
	 * 打印TDS的所有配置信息
	 * @return
	 */
	public static void printInfos()
	{
		if (tdsError)
			return;

		StringBuilder builder = new StringBuilder("\n\nCurrent Active Link Name Is : ").append(activeLinkName).append("\n");
		builder.append("All Link Informations (").append(linkInfos.size()).append(") Are Below :\n");

		linkInfos.forEach((k, v) -> {
			if ("DEFAULT".equals(k))
				return;

			v.getURL().ifPresent(url ->
				builder.append("Name : ").append(k).append("\n\t URL : ").append(url)
				.append("\n\t USR : ").append((v.getUsername()))
				.append("\n\t PWD : ").append((v.getPassword())).append("\n"));
		});

		System.out.println(builder.toString());
	}

	/**
	 * 获取所有的数据库连接信息
	 * @return
	 */
	public static Map<String, DbLinkInfo> getLinkInfos()
	{
		return linkInfos;
	}

	/**
	 * 获取某个数据库连接信息
	 * @param linkName
	 * @return
	 */
	public static DbLinkInfo getLinkInfo(String linkName)
	{
		return linkInfos.get(linkName);
	}

	/**
	 * 获取当前连接名称
	 * @return
	 */
	public static String getActiveLinkName()
	{
		return activeLinkName;
	}

	/**
	 * 设置当前连接名称
	 * 应当通过TdsSwitcher中的接口调用
	 * @param linkName
	 */
	static void setActiveLinkName(String linkName) throws IllegalArgumentException
	{
		if (StringUtil.isBlank(linkName) || !linkInfos.containsKey(linkName))
			throw new IllegalArgumentException("Invalid DB Link Name \"" + linkName + "\"...");

		activeLinkName = linkName;
	}

	/**
	 * 获取当前连接信息
	 * @return
	 */
	public static DbLinkInfo getActiveLinkInfo()
	{
		return linkInfos.get(activeLinkName);
	}

	/**
	 * 获取TDS初始化过程出错标志
	 * @return
	 */
	public static boolean isTdsError()
	{
		return tdsError;
	}

	/**
	 * 数据库连接信息类
	 */
	public static class DbLinkInfo
	{
		private String linkName = "";
		private String URL = "";
		private String username = "";
		private String password = "";

		public DbLinkInfo(String URL, String username, String password, String linkName)
		{
			this.URL = URL;
			this.username = username;
			this.password = password;
			this.linkName = linkName;
		}

		public String getLinkName()
		{
			return linkName;
		}

		public void setLinkName(String linkName)
		{
			this.linkName = linkName;
		}

		public Optional<String> getURL()
		{
			return Optional.ofNullable(URL);
		}

		public void setURL(String URL)
		{
			this.URL = URL;
		}

		public String getUsername()
		{
			return username;
		}

		public void setUsername(String username)
		{
			this.username = username;
		}

		public String getPassword()
		{
			return password;
		}

		public void setPassword(String password)
		{
			this.password = password;
		}
	}
}
