/**
 * @FileName TdsInitializeException.java
 * @Package com.fisheax.tds.core;
 * @author fisheax
 * @date 8/21/2016
 */
package com.fisheax.tds.core.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author fisheax
 * @ClassName: TdsInitializeException
 * @Description 运行中可能会产生的错误
 * @date 8/21/2016
 */
public class TdsSwitchException extends RuntimeException
{
	private static Logger logger = LoggerFactory.getLogger(TdsSwitchException.class);

	public TdsSwitchException()
	{
	}

	public TdsSwitchException(String message)
	{
		super(message);
		logger.error(message);
	}

	public TdsSwitchException(String message, Throwable cause)
	{
		super(message, cause);
		logger.error(message);
	}
}
