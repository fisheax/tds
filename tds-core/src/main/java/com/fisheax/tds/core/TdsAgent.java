/**
 * @FileName TdsAgent.java
 * @Package com.fisheax.tds.core;
 * @author fisheax
 * @date 8/21/2016
 */
package com.fisheax.tds.core;

import com.fisheax.tds.core.main.TdsConfig;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.instrument.ClassDefinition;
import java.lang.instrument.Instrumentation;
import java.sql.DriverManager;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author fisheax
 * @ClassName: TdsAgent
 * @Description 重定义DriverManager, 以拦截Driver来达到控制所有的Connection对象的目的
 * @date 8/21/2016
 */
public class TdsAgent
{
	private static Logger logger = LoggerFactory.getLogger(TdsAgent.class);

	public static void premain(String args, Instrumentation instrumentation)
	{
		logger.info(logo());
		redefineDriverManager(instrumentation);
		// be careful here, should let DriverManager init first
		// because of in TdsConfig#init() using Driver to check link's validatity
		TdsConfig.init();
	}

	private static String logo()
	{
		return
			"\n  /\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  /\\\\\\\\\\\\\\\\\\\\\\\\        /\\\\\\\\\\\\\\\\\\\\\\   \n" +
			"  \\///////\\\\\\/////  \\/\\\\\\////////\\\\\\    /\\\\\\/////////\\\\\\ \n" +
			"         \\/\\\\\\       \\/\\\\\\      \\//\\\\\\  \\//\\\\\\      \\///  \n" +
			"          \\/\\\\\\       \\/\\\\\\       \\/\\\\\\   \\////\\\\\\         \n" +
			"           \\/\\\\\\       \\/\\\\\\       \\/\\\\\\      \\////\\\\\\      \n" +
			"            \\/\\\\\\       \\/\\\\\\       \\/\\\\\\         \\////\\\\\\   \n" +
			"             \\/\\\\\\       \\/\\\\\\       /\\\\\\   /\\\\\\      \\//\\\\\\  \n" +
			"              \\/\\\\\\       \\/\\\\\\\\\\\\\\\\\\\\\\\\/   \\///\\\\\\\\\\\\\\\\\\\\\\/    \n" +
			"               \\///        \\////////////       \\///////////     \n\n" +
			"   ===============  MADE BY FISHEAX@GMAIL.COM  ==============\n";
	}

	/**
	 * 重定义DriverManager拦截Driver, 获取所有的Connection对象
	 * @param instrumentation
	 */
	private static void redefineDriverManager(Instrumentation instrumentation)
	{
		try
		{
			ClassPool pool = ClassPool.getDefault();
			pool.importPackage("com.fisheax.tds.core.main.DriverHandler");
			CtClass manager = pool.get("java.sql.DriverManager");

			CtMethod[] methods = manager.getDeclaredMethods();
			CtMethod registerDriverMethod = Arrays.asList(methods).stream()
				.filter(m -> "registerDriver".equals(m.getName()))
				.filter(m -> {
					try
					{
						// in jdk 8, there are two registerDriver, only need to intercept the new one
						// if not, the logger will log twice for one driver as if it was wrong
						return "1.8".equals(System.getProperty("java.specification.version")) ?
							m.getParameterTypes().length > 1 : true;
					}
					catch (NotFoundException e) { return false; }
				})
				.collect(Collectors.toList())
				.get(0);
			// intercept all drivers
			registerDriverMethod.insertBefore("$1 = DriverHandler.getProxiedDriver($1);");

			ClassDefinition definition = new ClassDefinition(DriverManager.class, manager.toBytecode());
			instrumentation.redefineClasses(definition);
		}
		catch (Exception e)
		{
			TdsConfig.setTdsError(true);
			logger.error("Error While Redefine DriverManager Class, Exiting TDS ...\n", e);
		}
	}
}
