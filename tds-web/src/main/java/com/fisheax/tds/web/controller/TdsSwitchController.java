/**
 * @FileName TdsSwitchController.java
 * @Package com.fisheax.tds.web.controller;
 * @Author fisheax
 * @Date 9/7/2016
 */
package com.fisheax.tds.web.controller;

import com.fisheax.tds.core.main.TdsConfig;
import com.fisheax.tds.core.main.TdsSwitcher;
import tk.lx.annotation.ActionMapping;
import tk.lx.annotation.RequestParam;
import tk.lx.dao.BaseDao;
import tk.lx.servlet.BaseHttpServlet;

import javax.servlet.annotation.WebServlet;
import java.io.IOException;

/**
 * @ClassName: TdsSwitchController
 * @Description TDS连接切换接口
 * @Author fisheax
 * @Date 9/7/2016
 */
@WebServlet(urlPatterns = "/switch/*")
public class TdsSwitchController extends BaseHttpServlet
{
	/**
	 * 显示所有在注册的TDS连接名称
	 */
	@ActionMapping("list")
	public void showAllLinks()
	{
		setAttr("linkInfos", TdsConfig.getLinkInfos());
		setAttr("activeLink", TdsConfig.getActiveLinkName());
		setAttr("students", BaseDao.getInstance().selectList("select * from student_info"));
		renderPage("/WEB-INF/view/list.jsp");
	}

	/**
	 * 切换至指定连接
	 * @param linkName
	 */
	@ActionMapping("to")
	public void switchLink(@RequestParam("name") String linkName) throws IOException
	{
		if (linkName.equals(TdsConfig.getActiveLinkName()))
		{
			renderJSONResult(-1, "The specific link is active, no need to switch ...");
			return;
		}

		if (TdsConfig.getLinkInfo(linkName) == null)
		{
			renderJSONResult(-2, "The specific link is invalid, do not switch ...");
			return;
		}

		TdsSwitcher.switchTo(linkName);
		renderJSONResult(0, "The active link is switched to the specific link ...");
	}
}
