/**
 * @FileName BaseService.java
 * @Package tk.lx.service
 * @author muxue
 * @date Nov 14, 2015
 */
package tk.lx.service;

import tk.lx.util.BeanFactoryUtil;


/**
 * @ClassName: BaseService
 * @Description 基础service层
 * @author muxue
 * @date Nov 14, 2015
 */
public abstract class BaseService
{
	public BaseService()
	{
		BeanFactoryUtil.injectFields(this);
	}
}
