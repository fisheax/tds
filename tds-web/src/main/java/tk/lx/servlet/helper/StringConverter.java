/**
 * @FileName StringConverter.java
 * @Package tk.lx.servlet.helper
 * @author muxue
 * @date Dec 12, 2015
 */
package tk.lx.servlet.helper;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import tk.lx.util.ValidateUtil;

/**
 * @ClassName: StringConverter
 * @Description String转换成其他常用的类型
 * @author muxue
 * @date Dec 12, 2015
 */
public class StringConverter
{
	// 默认的类型值
	private static HashMap<Class<?>, Object> defaultValue = new HashMap<Class<?>, Object>()
	{
		private static final long serialVersionUID = 1L;

		{
			put(String.class, "");
			
			put(Byte.class, (byte) 0);
			put(byte.class, (byte) 0);
			put(Short.class, (short) 0);
			put(short.class, (short) 0);
			put(Integer.class, 0);
			put(int.class, 0);
			put(Long.class, 0L);
			put(long.class, 0L);
			put(BigInteger.class, BigInteger.ZERO);

			put(Float.class, 0.0f);
			put(float.class, 0.0f);
			put(Double.class, 0.0);
			put(double.class, 0.0);
			put(BigDecimal.class, BigDecimal.ZERO);

			put(Date.class, Date.valueOf("1970-1-1"));
			put(Time.class, Time.valueOf("00:00:00"));
			put(Timestamp.class, Timestamp.valueOf("1970-1-1 00:00:00"));
		}
	};
	// 状态，转换是否成功。失败时，str2*()的返回值置为默认值，但要以此标志为准
	public static boolean isConvertSucceed = true;
	
	/**
	 * 根据Class对象获取相应类型的默认值
	 */
	public static Object getDefaultValue(Class<?> clazz)
	{
		return defaultValue.get(clazz);
	}

	// =========================================================================================
	// 将String类型值转换成需要的类型
	// =========================================================================================

	/**
	 * string转object(just an interface ...)
	 * 如果不是常见类型，则直接返回原来的string
	 */
	public static Object str2object(String value, Class<?> clazz)
	{
		if (clazz == null)
			return getDefaultValue(clazz);

		switch (clazz.getName())
		{
			case "java.lang.String":
				isConvertSucceed = true;
				return value;
				
			case "byte":
			case "java.lang.Byte":
				return str2byte(value);

			case "short":
			case "java.lang.Short":
				return str2short(value);

			case "int":
			case "java.lang.Integer":
				return str2int(value);

			case "long":
			case "java.lang.Long":
				return str2long(value);

			case "java.math.BigInteger":
				return str2bigint(value);

			case "float":
			case "java.lang.Float":
				return str2float(value);

			case "double":
			case "java.lang.Double":
				return str2double(value);

			case "java.math.BigDecimal":
				return str2decimal(value);

			case "java.sql.Date":
				return str2date(value);

			case "java.sql.Time":
				return str2time(value);

			case "java.sql.Timestamp":
				return str2stamp(value);

			default:
				return getDefaultValue(clazz);
		}
	}

	/**
	 * string转byte
	 */
	public static byte str2byte(String value)
	{
		if (ValidateUtil.validateByte(value)) 
		{
			isConvertSucceed = true;
			return Byte.parseByte(value);
		}
		else 
		{
			isConvertSucceed = false;
			return (byte) getDefaultValue(byte.class);
		}
	}

	/**
	 * string转short
	 */
	public static short str2short(String value)
	{
		if (ValidateUtil.validateShort(value)) 
		{
			isConvertSucceed = true;
			return Short.parseShort(value);
		}
		else 
		{
			isConvertSucceed = false;
			return (short) getDefaultValue(short.class);
		}
	}

	/**
	 * string转int
	 */
	public static int str2int(String value)
	{
		if (ValidateUtil.validateInteger(value)) 
		{
			isConvertSucceed = true;
			return Integer.parseInt(value);
		}
		else 
		{
			isConvertSucceed = false;
			return (int) getDefaultValue(int.class);
		}
	}

	/**
	 * string转long
	 */
	public static long str2long(String value)
	{
		if (ValidateUtil.validateLong(value)) 
		{
			isConvertSucceed = true;
			return Long.parseLong(value);
		}
		else 
		{
			isConvertSucceed = false;
			return (long) getDefaultValue(long.class);
		}
	}

	/**
	 * string转BigInteger
	 */
	public static BigInteger str2bigint(String value)
	{
		if (ValidateUtil.validateBigInteger(value)) 
		{
			isConvertSucceed = true;
			return new BigInteger(value);
		}
		else 
		{
			isConvertSucceed = false;
			return (BigInteger) getDefaultValue(BigInteger.class);
		}
	}

	/**
	 * string转float
	 */
	public static float str2float(String value)
	{
		if (ValidateUtil.validateFloat(value)) 
		{
			isConvertSucceed = true;
			return Float.parseFloat(value);
		}
		else 
		{
			isConvertSucceed = false;
			return (float) getDefaultValue(float.class);
		}
	}

	/**
	 * string转double
	 */
	public static double str2double(String value)
	{
		if (ValidateUtil.validateDouble(value)) 
		{
			isConvertSucceed = true;
			return Double.parseDouble(value);
		}
		else 
		{
			isConvertSucceed = false;
			return (double) getDefaultValue(double.class);
		}
	}

	/**
	 * string转BigDecimal
	 */
	public static BigDecimal str2decimal(String value)
	{
		if (ValidateUtil.validateBigDecimal(value)) 
		{
			isConvertSucceed = true;
			return new BigDecimal(value);
		}
		else 
		{
			isConvertSucceed = false;
			return (BigDecimal) getDefaultValue(BigDecimal.class);
		}
	}

	/**
	 * string转date，使用yyyy-MM-dd的格式
	 */
	public static Date str2date(String value)
	{
		if (ValidateUtil.validateDate(value)) 
		{
			isConvertSucceed = true;
			try
			{
				return new Date(new SimpleDateFormat("yyyy-MM-dd").parse(value).getTime());
			}
			catch (ParseException e)
			{
				return (Date) defaultValue.get(Date.class);
			}
		}
		else 
		{
			isConvertSucceed = false;
			return (Date) defaultValue.get(Date.class);
		}
	}

	/**
	 * string转time
	 */
	public static Time str2time(String value)
	{
		if (ValidateUtil.validateTime(value)) 
		{
			isConvertSucceed = true;
			return Time.valueOf(value);
		}
		else 
		{
			isConvertSucceed = false;
			return (Time) defaultValue.get(Time.class);
		}
	}

	/**
	 * string转timestamp
	 */
	public static Timestamp str2stamp(String value)
	{
		if (ValidateUtil.validateTimestamp(value)) 
		{
			isConvertSucceed = true;
			return Timestamp.valueOf(value);
		}
		else 
		{
			isConvertSucceed = false;
			return (Timestamp) defaultValue.get(Timestamp.class);
		}
	}

	// =========================================================================================

	/**
	 * do some test to make something clear
	 */
//	public static void main(String[] args)
//	{
//
//		System.out.println(Byte.class.getName());
//		System.out.println(byte.class.getName());
//		System.out.println(Byte.class == byte.class); // false
//
//		System.out.println(Short.class.getName());
//		System.out.println(short.class.getName());
//		System.out.println(Short.class == short.class); // false
//
//		System.out.println(Integer.class.getName());
//		System.out.println(int.class.getName());
//		System.out.println(Integer.class == int.class); // false
//
//		System.out.println(Long.class.getName());
//		System.out.println(long.class.getName());
//		System.out.println(Long.class == long.class); // false
//
//		System.out.println(BigInteger.class.getName());
//
//		System.out.println(Float.class.getName());
//		System.out.println(float.class.getName());
//		System.out.println(Float.class == float.class); // false
//
//		System.out.println(Double.class.getName());
//		System.out.println(double.class.getName());
//		System.out.println(Double.class == double.class); // false
//
//		System.out.println(BigDecimal.class.getName());
//
//		System.out.println(Date.class.getName());
//		System.out.println(Time.class.getName());
//		System.out.println(Timestamp.class.getName());
//		
//		System.out.println("===============================================================");
//		
//		Date date = Date.valueOf("1970-1-1");
//		Time time = Time.valueOf("00:00:00");
//		Timestamp timestamp = Timestamp.valueOf("1970-1-1 00:00:00");
//		System.out.println(date);
//		System.out.println(time);
//		System.out.println(timestamp);
//	}
}
