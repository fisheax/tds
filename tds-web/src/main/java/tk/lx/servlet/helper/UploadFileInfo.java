/**
 * @FileName UploadFileInfo.java
 * @Package tk.lx.servlet.helper
 * @author muxue
 * @date Dec 4, 2015
 */
package tk.lx.servlet.helper;

/**
 * @ClassName: UploadFileInfo
 * @Description 上传的文件信息
 * @author muxue
 * @date Dec 4, 2015
 */
public class UploadFileInfo
{
	//文件名（处理后的名称）
	private String name = null;
	//原文件名称
	private String originalName = null;
	//文件大小
	private long size = 0;
	//文件类型，同时也是文件的扩展名，包括dot
	private String type = null;
	//文件的URL地址，绝对路径
	private String url = null;
	
	public UploadFileInfo()
	{
	}
	
	public UploadFileInfo(String name, String originalName, long size, String type, String url)
	{
		this.name = name;
		this.originalName = originalName;
		this.size = size;
		this.type = type;
		this.url = url;
	}
	
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getOriginalName()
	{
		return originalName;
	}
	public void setOriginalName(String originalName)
	{
		this.originalName = originalName;
	}
	public long getSize()
	{
		return size;
	}
	public void setSize(long size)
	{
		this.size = size;
	}
	public String getType()
	{
		return type;
	}
	public void setType(String type)
	{
		this.type = type;
	}
	public String getUrl()
	{
		return url;
	}
	public void setUrl(String url)
	{
		this.url = url;
	}

	@Override
	public String toString()
	{
		return "UploadFileInfo [name=" + name + ", originalName=" + originalName + ", size=" + size + ", type=" + type + ", url=" + url + "]";
	}
	
}
