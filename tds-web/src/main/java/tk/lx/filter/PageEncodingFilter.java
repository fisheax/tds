package tk.lx.filter;

/**
 * 中文编码的过滤器
 * 
 * @author lx
 */

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

@WebFilter("/*")
public class PageEncodingFilter implements Filter
{

	@Override
	public void init(FilterConfig filterConfig) throws ServletException
	{
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
	{
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		//禁止浏览器缓存 --> 但是好像没起什么作用，先留着
		((HttpServletResponse)response).setHeader("Cache-Control", "no-cache"); 
		((HttpServletResponse)response).setDateHeader("Expires", -1);
		((HttpServletResponse)response).setHeader("Pragma","no-cache");

		chain.doFilter(request, response);
	}

	@Override
	public void destroy()
	{
	}

}
