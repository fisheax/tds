/**
 * @FileName IResultMapping.java
 * @Package tk.lx.dao.helper
 * @author muxue
 * @date Nov 11, 2015
 */
package tk.lx.dao.mapping;

/**
 * @ClassName: IResultMapping
 * @Description 
 * @author muxue
 * @date Nov 11, 2015
 */
public interface IResultMapping
{
	String getCoName(String columnName);
}
