/**
 * @FileName IRuleChecker.java
 * @Package tk.lx.permission
 * @author muxue
 * @date Nov 25, 2015
 */
package tk.lx.permission;

import tk.lx.servlet.BaseHttpServlet;

/**
 * @ClassName: IRuleChecker
 * @Description 权限检查者接口
 * @author muxue
 * @date Nov 25, 2015
 */
public interface IRuleChecker
{
	boolean checkRule(String rule, BaseHttpServlet servlet);
}
