/**
 * @FileName EscapeUtil.java
 * @Package tk.lx.util
 * @author muxue
 * @date Jan 8, 2016
 */
package tk.lx.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: EscapeUtil
 * @Description 编码某些特殊的字符以避免带来某些问题
 * @author muxue
 * @date Jan 8, 2016
 */
public class EscapeUtil
{
	/**
	 * 编码HTML代码中某些危险的字符
	 */
	public static String escapeHtml(String htmlText)
	{
		HashMap<String, String> escapeMap = new HashMap<String, String>()
		{
			private static final long serialVersionUID = 1L;

			{
				// may here be more escape ...
				put("<", "&lt;");
				put(">", "&gt;");
			}
		};
		
		for (Map.Entry<String, String> entry : escapeMap.entrySet())
			htmlText = htmlText.replaceAll(entry.getKey(), entry.getValue());
			
		return htmlText;
	}
	
	
	/**
	 * 编码sql语句 
	 */
	public static String escapeSql(String sqlText)
	{
		// 暂时占个位置，关于本类的escape其实都还没有考虑好
		return sqlText;
	}
	
	// may here be more escape things except html ...
}
