package tk.lx.util;

import java.lang.reflect.Field;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import tk.lx.util.helper.JsonParseHelper;

/**
 * ClassName: JsonUtil
 * Description: 用于JSON化数据
 * Date: Oct 31, 2015
 *
 * @author muxue
 */

public class JsonUtil
{

	/**
	 * JSON化对外接口
	 */
	@SuppressWarnings("unchecked")
	public static String stringify(Object object)
	{
		if (object == null)
			return "null";
		
		else if(object instanceof Boolean || object instanceof Float || object instanceof Double
				|| object instanceof Integer || object instanceof Long || object instanceof Short)
			return object.toString();
		
		else if (object instanceof Date || object instanceof Timestamp ||
				object instanceof Time|| object instanceof String) 
			return "\"" + object.toString() + "\"";
		
		else if (object instanceof Collection)
			return stringifyCollection((Collection<Object>) object);
		
		else if (object instanceof Map)
			return stringifyMap((Map<String, Object>) object);
		
		else 
			return stringifyObject(object);
	}

	/**
	 * JSON化非Collection和非Map的对象
	 */
	@SuppressWarnings("unchecked")
	private static String stringifyObject(Object object)
	{
		if (object == null)
			return "";

		StringBuilder json = new StringBuilder("{");

		Field[] fields = object.getClass().getDeclaredFields();
		try
		{
			for (Field field : fields)
			{
				field.setAccessible(true);

				Object obx = field.get(object);
				json.append("\"").append(field.getName()).append("\":");

				if (obx instanceof Collection)
					json.append(stringifyCollection((Collection<Object>) obx)).append(",");
				else if (obx instanceof Map)
					json.append(stringifyMap((Map<String, Object>) obx)).append(",");
				else if (isInstanceOfSimpleClass(obx))
				{
					json.append(obx == null ? "null" : (obx instanceof String || obx instanceof Date 
							|| obx instanceof Time || obx instanceof Timestamp
							? "\"" + obx.toString() + "\"" : obx.toString()));
					json.append(",");
				}
				else
					json.append(stringifyObject(obx)).append(",");
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			return "";
		}
		if (json.toString().endsWith(",")) 
			json.replace(json.length() - 1, json.length(), "}");
		else 
			json.append('}');
		
		return json.toString();
	}

	/**
	 * JSON化一般的Collection类
	 */
	@SuppressWarnings("unchecked")
	private static String stringifyCollection(Collection<Object> collection)
	{
		if (collection == null)
			return "";

		StringBuilder json = new StringBuilder("[");
		Iterator<Object> iter = (Iterator<Object>) collection.iterator();

		while (iter.hasNext())
		{
			Object object = iter.next();

			if (object instanceof Collection)
				json.append(stringifyCollection((Collection<Object>) object)).append(",");
			else if (object instanceof Map)
				json.append(stringifyMap((Map<String, Object>) object)).append(",");
			else if (isInstanceOfSimpleClass(object))
			{
				json.append(object == null ? "null" : (object instanceof String || object instanceof Date 
						|| object instanceof Time || object instanceof Timestamp
						? "\"" + object.toString() + "\"" : object.toString()));
				json.append(",");
			}
			else
				json.append(stringifyObject(object)).append(",");
		}
		if (json.toString().endsWith(",")) 
			json.replace(json.length() - 1, json.length(), "]");
		else 
			json.append(']');

		return json.toString();
	}

	/**
	 * JSON化一般的Map类
	 */
	@SuppressWarnings("unchecked")
	private static String stringifyMap(Map<String, Object> map)
	{
		if (map == null)
			return null;

		StringBuffer json = new StringBuffer("{");

		for (Entry<String, Object> entry : map.entrySet())
		{
			json.append("\"").append(entry.getKey()).append("\":");
			
			Object object = entry.getValue();
			if (object instanceof Collection)
				json.append(stringifyCollection((Collection<Object>) object)).append(",");
			else if (object instanceof Map)
				json.append(stringifyMap((Map<String, Object>) object)).append(",");
			else if (isInstanceOfSimpleClass(object))
			{
				json.append(object == null ? "null" : (object instanceof String || object instanceof Date 
														|| object instanceof Time || object instanceof Timestamp
														? "\"" + object.toString() + "\"" : object.toString()));
				json.append(",");
			}
			else
				json.append(stringifyObject(object)).append(",");
		}

		if (json.toString().endsWith(",")) 
			json.replace(json.length() - 1, json.length(), "}");
		else 
			json.append('}');

		return json.toString();
	}

	// ==================================================================================

	/**
	 * 将JSON数据转成list --> 强制转换类型到ArrayList
	 */
	@SuppressWarnings("unchecked")
	public static ArrayList<Object> parseJsonToList(String jsonText)
	{
		return (ArrayList<Object>) new JsonParseHelper().parseJson(jsonText);
	}

	/**
	 * 将JSON数据转换成map --> 强制转换类型到HashMap
	 */
	@SuppressWarnings("unchecked")
	public static HashMap<String, Object> parseJsonToMap(String jsonText)
	{
		return (HashMap<String, Object>) new JsonParseHelper().parseJson(jsonText);
	}

	/**
	 * 将JSON数据转换成具有相应属性的对象 --> 当然如果不是bean结构的JSON的话肯定是不会成功的
	 */
	public static <T> T parseJsonToBean(String jsonText, Class<T> clazz)
	{
		return mapToBean(parseJsonToMap(jsonText), clazz);
	}

	/**
	 * 将JSON数据转换成具有相应属性的对象
	 */
	@SuppressWarnings("unchecked")
	public static <T> ArrayList<T> parseJsonToBeans(String jsonText, Class<?> clazz)
	{
		ArrayList<T> beans = new ArrayList<>();
		ArrayList<Object> jsonObjects = parseJsonToList(jsonText);

		for (Object object : jsonObjects)
			beans.add((T) mapToBean((HashMap<String, Object>) object, clazz));

		return beans;
	}

	// ==================================================================================
	// helper

	/**
	 * 是否是简单类的实例，就是几个简单值：string, true, false, number, null
	 */
	private static boolean isInstanceOfSimpleClass(Object object)
	{
		// 下面列出来的类都是mysql数据类型对应java里的类型
		return (object == null || object instanceof String || object instanceof Integer 
			|| object instanceof Boolean || object instanceof Long || object instanceof Float 
			|| object instanceof Double || object instanceof Short || object instanceof Date
			|| object instanceof Timestamp || object instanceof Time);
	}

	/**
	 * 将一个HashMap转换成具有相应属性的对象，用于上下两个方法
	 */
	private static <T> T mapToBean(HashMap<String, Object> map, Class<T> clazz)
	{
		if (clazz == null)
			return null;

		T bean = null;
		boolean ifAccessed = false;
		try
		{
			bean = clazz.newInstance();
			for (String keyname : map.keySet())
			{
				Field[] fields = bean.getClass().getDeclaredFields();
				for (Field field : fields)
				{
					if (field.getName().equals(keyname))
					{
						field.setAccessible(true);
						convertTypeAndSet(bean, field, map.get(keyname));
						ifAccessed = true;
					}
				}
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			return null;
		}

		return ifAccessed ? bean : null;
	}

	/**
	 * 将json中number类型（long，double）转换为bean对象所需的类型
	 * 
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	private static <T> void convertTypeAndSet(T bean, Field field, Object value) throws IllegalArgumentException, IllegalAccessException
	{
		String typeName = field.getType().getName();

		if (value instanceof Long)
			switch (typeName)
			{
				case "java.lang.Long":
				case "long":
					field.set(bean, value);
					break;
				case "java.lang.Integer":
				case "int":
					field.set(bean, ((Long)value).intValue());
					break;
				case "java.lang.Short":
				case "short":
					field.set(bean, ((Long)value).shortValue());
					break;
			}
		else if (value instanceof Double)
			switch (typeName)
			{
				case "java.lang.Double":
				case "double":
					field.set(bean, value);
					break;
				case "java.lang.Float":
				case "float":
					field.set(bean, ((Double)value).floatValue());
					break;
			}
		else
			field.set(bean, value);
	}
}
