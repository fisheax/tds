<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
	<title>INFO</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/style.css">
</head>
<body>
<table border="1">
	<thead align="center">ALL LINKS INFO <span id="msg"></span></thead>
	<tr>
		<td>LINK NAME</td>
		<td>LINK URL</td>
		<td>LINK USR</td>
		<td>LINK PWD</td>
		<td>SWITCH</td>
	</tr>
	<c:forEach var="link" items="${linkInfos}">
		<tr class="${activeLink eq link.value.linkName ? 'active' : ''}">
			<td>${link.value.linkName}</td>
			<td>${fn:substring(link.value.URL, 9, fn:indexOf(link.value.URL, "]"))}</td>
			<td>${link.value.username}</td>
			<td>${link.value.password}</td>
			<td><button>switch</button></td>
		</tr>
	</c:forEach>
</table>
<br>
<table border="1">
	<thead align="center">ALL STUDENTS INFO</thead>
	<tbody>
	<tr>
		<td>id</td>
		<td>name</td>
		<td>number</td>
		<td>time</td>
	</tr>
	<c:forEach var="student" items="${students}">
		<tr>
			<td>${student.studentId}</td>
			<td>${student.studentName}</td>
			<td>${student.studentNumber}</td>
			<td>${student.studentTime}</td>
		</tr>
	</c:forEach>
	</tbody>
</table>
<script>
	window._context_path_ = '${pageContext.request.contextPath}';
</script>
<script src="${pageContext.request.contextPath}/resource/jquery-2.2.4.min.js"></script>
<script src="${pageContext.request.contextPath}/resource/switch.js"></script>
</body>
</html>
