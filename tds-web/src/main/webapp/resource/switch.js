$(function(){
    $('button').click(function () {
        var curRow = $(this).parent().parent();
        var linkName = curRow.find('td').first().text();
        $.get(window._context_path_ + '/switch/to', {name: linkName}, function (resp) {
            alert(resp.errMsg);
            location.reload();
        });
    })
});
